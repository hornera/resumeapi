﻿CREATE TABLE dbo.Profile(
	ID INT IDENTITY(1000,1),
	FirstName NVARCHAR(255) NOT NULL,
	LastName NVARCHAR(255) NOT NULL,
	CarreerObjective NVARCHAR(2048)
	CONSTRAINT[PK_dbo.Profiles] PRIMARY KEY CLUSTERED (ID ASC)
);

CREATE TABLE dbo.Education(
	ID INT IDENTITY(0,1),
	UserID INT,
	School NVARCHAR(255),
	StartDate DATETIME NOT NULL,
	EndData DATETIME NOT NULL,
	GPA DECIMAL(3,2),
	CONSTRAINT[PK_dbo.Education] PRIMARY KEY CLUSTERED (ID ASC),
	CONSTRAINT[FK_dbo.ProfileEducation] FOREIGN KEY (UserID)
		REFERENCES dbo.Profile(ID)
		ON DELETE CASCADE
);

CREATE TABLE dbo.Employment(
	ID INT IDENTITY(0,1),
	UserID INT, 
	Company NVARCHAR(255) NOT NULL,
	StartDate DATETIME NOT NULL,
	EndDate DATETIME,
	Description NVARCHAR(1024),
	CONSTRAINT[PK_dbo.Employment] PRIMARY KEY CLUSTERED (ID ASC),
	CONSTRANT[FK_dbo.ProfileEmployment] FOREIGN KEY (UserID)
		REFERENCES dbo.Profile(ID)
		ON DELETE CASCADE		
);

CREATE TABLE dbo.Volunteer(
	ID INT IDENTITY(0,1),
	UserID INT,
	Organization NVARCHA(255) NOT NULL,
	StartDate DATETIME NOT NULL,
	EndDate DATETIME,
	Description NVARCHAR(1024),
	CONSTRAINT [PK_dbo.Volunteer] PRIMARY KEY CLUSTERED (ID ASC),
	CONSTRAINT[FK_dbo.ProfileVolunteer] FOREIGN KEY (UserID)
		REFERENCES dbo.Profile(ID)
		ON DELETE CASCADE
);

CREATE TABLE dbo.Skill(
    ID INT IDENTITY(0,1),
    UserID INT,
    SkillName NVARCHAR(255),
    CONSTRAINT[PK_dbo.Skill] PRIMARY KEY CLUSTERED (ID ASC),
    CONSTRAINT[FK_dbo.ProfileSkill] FOREIGN KEY (UserID)
        REFERENCES dbo.Profile(ID)
        ON DELETE CASCADE
);

CREATE TABLE dbo.Course(
    ID INT IDENTITY(0,1),
    UserID INT,
    SchoolID INT,
    CourseNumber NVARCHAR(10) NOT NULL,
    CourseTitle NVARCHAR(255) NOT NULL,
    CourseDescription NVARCHAR(1024) NOT NULL,
    Grade NVARCHAR(2) NOT NULL,
    CONSTRAINT[PK_dbo.Course] PRIMARY KEY CLUSTERED (ID ASC),
    CONSTRAINT[FK_dbo.ProfileCourse] FOREIGN KEY (UserID)
        REFERENCES dbo.Profile(ID)
        ON DELETE CASCADE,
    CONSTRAINT[FK_dbo.SchoolCourse] FOREIGN KEY (SchoolID)
        REFERENCES dbo.School(ID)
        ON DELETE CASCADE
);

CREATE TABLE dbo.Project(
    ID INT IDENTITY(0,1),
    UserID INT,
    Name NVARCHAR(1024) NOT NULL,
    ProjectFocus NVARCHAR(1024) NOT NULL,
    ProjectLink NVARCHAR(255)NOT NULL,
    Description NVARCHAR(2048) NOT NULL,
    CONSTRAINT[PK_dbo.Project] PRIMARY KEY CLUSTERED (ID ASC),
    CONSTRAINT[FK_dbo.ProfileProject] FOREIGN KEY (UserID)
        REFERENCES dbo.Profile(ID)
        ON DELETE CASCADE
);

CREATE TABLE dbo.ProjectSkill(
    ID INT IDENTITY(0,1),
    SkillID INT,
    ProjectID INT,
    CONSTRAINT[PK_dbo.ProjectSkill] PRIMARY KEY CLUSTERED (ID ASC),
    CONSTRAINT[FK_dbo.ProjectSkill] FOREIGN KEY (SkillID)
        REFERENCES dbo.Skill(ID)
        ON DELETE CASCADE,
    CONSTRAINT[FK_dbo.ProjectSkillProject] FOREIGN KEY (ProjectID)
        REFERENCES dbo.Project(ID)
        ON DELETE CASCADE
)